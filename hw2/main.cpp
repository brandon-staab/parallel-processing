#include <mpi.h>

#include <cassert>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <string>


double factorial(const double x) {
	assert(x >= 0);

	if (x <= 1) {
		return 1;
	}

	return x * factorial(x - 1);
}


int main(int argc, char* argv[]) {
	int rank, comm_size;
	double x, sum, term, n;

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &comm_size);

	x = 0;
	sum = 0;

	if (rank == 0) {
		std::string line;

	input:
		std::cout << "Enter a number: ";
		std::getline(std::cin, line);

		try {
			x = std::stod(line);

			if (!(std::isnormal(x) || x == 0.0)) {
				throw true;
			}
		} catch (...) {
			std::cout << "Bad input.\n\n";
			goto input;
		}
	}

	MPI_Bcast(&x, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);

	n = rank + 1;
	term = std::pow(x, n + 1) / factorial(n + 1);

	MPI_Reduce(&term, &sum, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

	if (rank == 0) {
		sum += 1 + x;
		std::cout << "The Taylor series approximation of the first " << (comm_size + 3) << " terms of \"e^" << x << "\" is:\n"
				  << std::fixed << std::setprecision(52) << sum << std::endl;
	}

	MPI_Finalize();
}
