/**
 *	Brandon Staab<bls114@zips.uakron.edu>
 *	Parallel Processing
 *	Dr. O'Neil
 *	22 Nov 2018
 *
 *	Lab 4: Floyd's Algorithm
 */

#pragma once

#include "Matrix.hpp"

#include <algorithm>
#include <type_traits>


template <typename T, typename Weight>
class MatrixGraph {
	static_assert(std::is_arithmetic<Weight>::value, "Arithmetic type required for Weight");

  public:
	MatrixGraph(std::size_t const elements, Weight const& default_weight) noexcept;

	// Vertices
	std::size_t num_vertices() const noexcept;
	void add_vertex(T const& val) noexcept;
	T& get_vertex(std::size_t const vertex) noexcept;
	T const& get_vertex(std::size_t const vertex) const noexcept;

	// Edges
	void set_edge(std::size_t const from, std::size_t const to, Weight const weight) noexcept;
	Weight& get_edge(std::size_t const column_x, std::size_t const row_y) noexcept;
	Weight const& get_edge(std::size_t const column_x, std::size_t const row_y) const noexcept;

  private:
	Matrix<Weight> m_adj_mat;
	std::vector<T> m_vertices;
};


#include "MatrixGraph.tpp"
