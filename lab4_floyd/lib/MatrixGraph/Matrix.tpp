/**
 *	Brandon Staab<bls114@zips.uakron.edu>
 *	Parallel Processing
 *	Dr. O'Neil
 *	22 Nov 2018
 *
 *	Lab 4: Floyd's Algorithm
 */

#pragma once

#include <cassert>


template <typename T>
Matrix<T>::Matrix(std::size_t const cols_x, std::size_t const rows_y) noexcept : m_cols(cols_x), m_rows(rows_y), m_data(cols_x * rows_y) {}


template <typename T>
Matrix<T>::Matrix(std::size_t const cols_x, std::size_t const rows_y, T const& default_val) noexcept : m_cols(cols_x), m_rows(rows_y), m_data(cols_x * rows_y, default_val) {}


template <typename T>
constexpr std::size_t Matrix<T>::get_columns() const noexcept {
	return m_cols;
}


template <typename T>
constexpr std::size_t Matrix<T>::get_rows() const noexcept {
	return m_rows;
}


template <typename T>
constexpr std::size_t Matrix<T>::get_element_count() const noexcept {
	assert(m_cols * m_rows == m_data.size());
	return m_cols * m_rows;
}


template <typename T>
T* Matrix<T>::data() noexcept {
	return m_data.data();
}


template <typename T>
T const* Matrix<T>::data() const noexcept {
	return m_data.data();
}


template <typename T>
typename Matrix<T>::reference Matrix<T>::operator()(std::size_t const column_x, std::size_t const row_y) noexcept {
	assert(0 <= column_x);
	assert(column_x < get_columns());
	assert(0 <= row_y);
	assert(row_y < get_rows());

	return m_data[row_y * m_cols + column_x];
}


template <typename T>
typename Matrix<T>::const_reference Matrix<T>::operator()(std::size_t const column_x, std::size_t const row_y) const noexcept {
	assert(0 <= column_x);
	assert(column_x < get_columns());
	assert(0 <= row_y);
	assert(row_y < get_rows());

	return m_data[row_y * m_cols + column_x];
}
