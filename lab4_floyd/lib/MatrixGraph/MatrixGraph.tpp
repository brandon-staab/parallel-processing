/**
 *	Brandon Staab<bls114@zips.uakron.edu>
 *	Parallel Processing
 *	Dr. O'Neil
 *	22 Nov 2018
 *
 *	Lab 4: Floyd's Algorithm
 */

#pragma once


template <typename T, typename Weight>
MatrixGraph<T, Weight>::MatrixGraph(std::size_t const elements, Weight const& default_weight) noexcept : m_adj_mat(elements, elements, default_weight), m_vertices() {}

/******* Vertices ************/

template <typename T, typename Weight>
std::size_t MatrixGraph<T, Weight>::num_vertices() const noexcept {
	return m_vertices.size();
}


template <typename T, typename Weight>
void MatrixGraph<T, Weight>::add_vertex(T const& val) noexcept {
	assert(0 == std::count(m_vertices.begin(), m_vertices.end(), val));
	assert(m_vertices.size() < m_adj_mat.get_rows());
	assert(m_vertices.size() < m_adj_mat.get_columns());

	m_vertices.push_back(val);
}


template <typename T, typename Weight>
T& MatrixGraph<T, Weight>::get_vertex(std::size_t const vertex) noexcept {
	return m_vertices[vertex];
}


template <typename T, typename Weight>
T const& MatrixGraph<T, Weight>::get_vertex(std::size_t const vertex) const noexcept {
	return m_vertices[vertex];
}

/******* Edges ***************/

template <typename T, typename Weight>
void MatrixGraph<T, Weight>::set_edge(std::size_t const from, std::size_t const to, Weight const weight) noexcept {
	m_adj_mat(from, to) = weight;
}


template <typename T, typename Weight>
Weight& MatrixGraph<T, Weight>::get_edge(std::size_t const column_x, std::size_t const row_y) noexcept {
	return m_adj_mat(column_x, row_y);
}


template <typename T, typename Weight>
Weight const& MatrixGraph<T, Weight>::get_edge(std::size_t const column_x, std::size_t const row_y) const noexcept {
	return m_adj_mat(column_x, row_y);
}
