/**
 *	Brandon Staab<bls114@zips.uakron.edu>
 *	Parallel Processing
 *	Dr. O'Neil
 *	22 Nov 2018
 *
 *	Lab 4: Floyd's Algorithm
 */

#pragma once

#include <vector>


template <typename T>
class Matrix {
	using reference = typename std::vector<T>::reference;
	using const_reference = typename std::vector<T>::const_reference;

  public:
	Matrix(std::size_t const cols_x, std::size_t const rows_y) noexcept;
	Matrix(std::size_t const cols_x, std::size_t const rows_y, T const& default_val) noexcept;

	constexpr std::size_t get_columns() const noexcept;
	constexpr std::size_t get_rows() const noexcept;
	constexpr std::size_t get_element_count() const noexcept;

	T* data() noexcept;
	T const* data() const noexcept;

	reference operator()(std::size_t const column_x, std::size_t const row_y) noexcept;
	const_reference operator()(std::size_t const column_x, std::size_t const row_y) const noexcept;

  private:
	std::size_t const m_cols;
	std::size_t const m_rows;
	std::vector<T> m_data;
};


#include "Matrix.tpp"
