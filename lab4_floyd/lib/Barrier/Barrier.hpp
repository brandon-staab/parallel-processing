/**
 *	Brandon Staab<bls114@zips.uakron.edu>
 *	Parallel Processing
 *	Dr. O'Neil
 *	22 Nov 2018
 *
 *	Lab 4: Floyd's Algorithm
 */

#pragma once

#include <condition_variable>
#include <mutex>


class Barrier {
  public:
	explicit Barrier(std::size_t const count) noexcept;

	void wait() noexcept;

  private:
	std::size_t const m_threshold;
	std::size_t m_remaining;
	std::size_t m_generation;
	std::mutex m_mutex;
	std::condition_variable m_condition;
};
