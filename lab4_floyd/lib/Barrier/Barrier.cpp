/**
 *	Brandon Staab<bls114@zips.uakron.edu>
 *	Parallel Processing
 *	Dr. O'Neil
 *	22 Nov 2018
 *
 *	Lab 4: Floyd's Algorithm
 */

#include "Barrier.hpp"


Barrier::Barrier(std::size_t const count) noexcept : m_threshold(count), m_remaining(count), m_generation(0) {}


void Barrier::wait() noexcept {
	auto local = std::unique_lock<std::mutex>{m_mutex};
	auto current_generation = m_generation;

	m_remaining--;
	if (!m_remaining) {
		m_generation++;
		m_remaining = m_threshold;
		m_condition.notify_all();
	} else {
		m_condition.wait(local, [this, current_generation] { return current_generation != m_generation; });
	}
}
