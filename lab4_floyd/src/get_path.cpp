/**
 *	Brandon Staab<bls114@zips.uakron.edu>
 *	Parallel Processing
 *	Dr. O'Neil
 *	22 Nov 2018
 *
 *	Lab 4: Floyd's Algorithm
 */

#include "get_path.hpp"

#include "roy_floyd.hpp"

#include <thread>


namespace {
	void gen_path(std::size_t from, std::size_t const to, PathList& path, PredMat const& pred) noexcept {
		if (pred(from, to) == UNDEFINED) {
			if (path.empty()) {
				path.emplace_back(from);
			}

			path.emplace_back(to);
		} else {
			gen_path(from, pred(from, to), path, pred);
			gen_path(pred(from, to), to, path, pred);
		}
	}
}  // namespace


std::pair<PathList, Distance> get_path(CityGraph const& city_graph, std::size_t const from, std::size_t const to, std::size_t const num_threads) noexcept {
	assert(0 < num_threads);
	assert(num_threads <= city_graph.num_vertices());

	auto dist = DistanceMat{city_graph.num_vertices(), city_graph.num_vertices(), std::numeric_limits<double>::infinity()};
	auto pred = PredMat{city_graph.num_vertices(), city_graph.num_vertices(), UNDEFINED};

	std::vector<std::thread> threads;
	auto barrier = Barrier{num_threads};
	for (auto i = 0; i < num_threads; i++) {
		threads.emplace_back(roy_floyd, i, num_threads, std::ref(barrier), std::ref(city_graph), std::ref(dist), std::ref(pred));
	}

	for (auto& thread : threads) {
		thread.join();
	}

	auto path = PathList{};
	gen_path(from, to, path, pred);

	return {path, dist(from, to)};
}
