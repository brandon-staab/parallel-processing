/**
 *	Brandon Staab<bls114@zips.uakron.edu>
 *	Parallel Processing
 *	Dr. O'Neil
 *	22 Nov 2018
 *
 *	Lab 4: Floyd's Algorithm
 */

#pragma once

#include <cstddef>


std::size_t get_thead_amt(std::size_t const max_amt) noexcept;
