/**
 *	Brandon Staab<bls114@zips.uakron.edu>
 *	Parallel Processing
 *	Dr. O'Neil
 *	22 Nov 2018
 *
 *	Lab 4: Floyd's Algorithm
 */

#include "print_results.hpp"

#include <iomanip>
#include <iostream>


void print_results(CityGraph const& city_graph, PathList const& path, Distance const distance, double const milliseconds) noexcept {
	std::cout << "\n** " << city_graph.get_vertex(path.front()) << " to " << city_graph.get_vertex(path.back()) << " **\n\nShortest Route:\n";

	for (auto const& city_idx : path) {
		std::cout << city_graph.get_vertex(city_idx) << "\n";
	}

	std::cout << "\nTotal distance: " << std::setprecision(1) << std::fixed << distance << " miles\nThe search took: " << std::setprecision(4) << std::fixed << milliseconds
			  << " milliseconds\n\n- - - - - - - - - - - - - - - -\n"
			  << std::endl;
}
