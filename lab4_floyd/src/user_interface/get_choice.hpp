/**
 *	Brandon Staab<bls114@zips.uakron.edu>
 *	Parallel Processing
 *	Dr. O'Neil
 *	22 Nov 2018
 *
 *	Lab 4: Floyd's Algorithm
 */

#pragma once

#include <utility>


std::pair<std::size_t, std::size_t> get_choice(std::size_t num_cities) noexcept;
