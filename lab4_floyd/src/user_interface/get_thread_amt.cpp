/**
 *	Brandon Staab<bls114@zips.uakron.edu>
 *	Parallel Processing
 *	Dr. O'Neil
 *	22 Nov 2018
 *
 *	Lab 4: Floyd's Algorithm
 */

#include "get_choice.hpp"

#include <iostream>
#include <string>


std::size_t get_thead_amt(std::size_t const max_amt) noexcept {
	std::size_t num_threads;
	std::string line;

get_input:
	try {
		std::cout << "Thread amount? " << std::flush;
		std::cin >> line;

		num_threads = std::stoul(line);

		if (num_threads == 0) {
			std::cerr << "you must assign this program atleast one thread\n\n";
			goto get_input;
		}

		if (num_threads > max_amt) {
			std::cerr << "the number of threads can not exceed the amount of cities\n\n";
			goto get_input;
		}

		return num_threads;
	} catch (const std::exception& e) {
		std::cerr << "exception caught: " << e.what() << "\n\n";
		goto get_input;
	}
}
