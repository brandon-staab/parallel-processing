/**
 *	Brandon Staab<bls114@zips.uakron.edu>
 *	Parallel Processing
 *	Dr. O'Neil
 *	22 Nov 2018
 *
 *	Lab 4: Floyd's Algorithm
 */

#include "is_being_used.hpp"

#include <iostream>


bool is_being_used() noexcept {
	char choice;

get_choice:
	std::cout << "Another path (y/n)? " << std::flush;
	std::cin >> choice;
	if (choice == 'y') {
		return true;
	} else if (choice == 'n') {
		return false;
	} else {
		goto get_choice;
	}
}
