/**
 *	Brandon Staab<bls114@zips.uakron.edu>
 *	Parallel Processing
 *	Dr. O'Neil
 *	22 Nov 2018
 *
 *	Lab 4: Floyd's Algorithm
 */

#include "display_menu.hpp"

#include <iostream>


void display_menu(CityGraph const& city_graph) noexcept {
	std::cout << "NQMQ Menu\n- - - - -\n";

	for (auto i = 0; i < city_graph.num_vertices(); i++) {
		std::cout << i << ". " << city_graph.get_vertex(i) << '\n';
	}

	std::cout << "\n- - - - - - - - - - - - - - - -\n" << std::endl;
}
