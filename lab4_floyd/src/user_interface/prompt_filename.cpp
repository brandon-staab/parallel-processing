/**
 *	Brandon Staab<bls114@zips.uakron.edu>
 *	Parallel Processing
 *	Dr. O'Neil
 *	22 Nov 2018
 *
 *	Lab 4: Floyd's Algorithm
 */

#include "prompt_filename.hpp"

#include <fstream>
#include <iostream>


namespace {
	bool is_file_exist(const char* fileName) noexcept {
		auto file = std::ifstream{fileName};
		return file.good();
	}
}  // namespace


std::string prompt_filename(std::string filename) noexcept {
	while (!is_file_exist(filename.c_str())) {
		std::cout << filename << " does not exist.\n\nPlease enter a different filename: " << std::flush;
		std::cin >> filename;
	}

	return filename;
}
