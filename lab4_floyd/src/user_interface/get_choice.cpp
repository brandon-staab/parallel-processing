/**
 *	Brandon Staab<bls114@zips.uakron.edu>
 *	Parallel Processing
 *	Dr. O'Neil
 *	22 Nov 2018
 *
 *	Lab 4: Floyd's Algorithm
 */

#include "get_choice.hpp"

#include <iostream>
#include <string>


std::pair<std::size_t, std::size_t> get_choice(std::size_t num_cities) noexcept {
	std::size_t from;
	std::size_t to;
	std::string line;

get_from:
	try {
		std::cout << "Path from? " << std::flush;
		std::cin >> line;
		from = std::stoull(line);
		if (from > num_cities) {
			std::cerr << "entered city is greater that the number of cities\n\n";
			goto get_from;
		}
	} catch (const std::exception& e) {
		std::cerr << "exception caught: " << e.what() << "\n\n";
		goto get_from;
	}

get_to:
	try {
		std::cout << "Path to? " << std::flush;
		std::cin >> line;
		to = std::stoull(line);
		if (to > num_cities) {
			std::cerr << "entered city is greater that the number of cities\n\n";
			goto get_to;
		}
	} catch (const std::exception& e) {
		std::cerr << "exception caught: " << e.what() << "\n\n";
		goto get_to;
	}

	return {from, to};
}
