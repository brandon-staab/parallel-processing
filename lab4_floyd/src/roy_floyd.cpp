/**
 *	Brandon Staab<bls114@zips.uakron.edu>
 *	Parallel Processing
 *	Dr. O'Neil
 *	22 Nov 2018
 *
 *	Lab 4: Floyd's Algorithm
 */

#include "roy_floyd.hpp"


namespace {
	void init(CityGraph const& graph, DistanceMat& dist, DistanceMat& temp_dist, std::size_t const start_y, std::size_t const end_y) noexcept {
		for (size_t y = start_y; y < end_y; y++) {
			for (size_t x = 0; x < graph.num_vertices(); x++) {
				dist(x, y) = temp_dist(x, y - start_y) = graph.get_edge(x, y);
			}
		}
	}


	void calculate_distance(DistanceMat const& dist,
							DistanceMat& temp_dist,
							std::size_t const n,
							std::size_t const start_y,
							std::size_t const end_y,
							PredMat& pred,
							std::size_t const k) noexcept {
		for (auto y = start_y; y < end_y; y++) {
			for (size_t x = 0; x < n; x++) {
				if (dist(x, y) > dist(x, k) + dist(k, y)) {
					temp_dist(x, y - start_y) = dist(x, k) + dist(k, y);
					pred(x, y) = k;
				}
			}
		}
	}


	void update_distance(DistanceMat& dist, DistanceMat const& temp_dist, std::size_t const n, std::size_t const start_y, std::size_t const end_y) noexcept {
		for (auto y = start_y; y < end_y; y++) {
			for (size_t x = 0; x < n; x++) {
				dist(x, y) = temp_dist(x, y - start_y);
			}
		}
	}
}  // namespace


void roy_floyd(std::size_t const my_rank, std::size_t const num_threads, Barrier& barrier, CityGraph const& graph, DistanceMat& dist, PredMat& pred) noexcept {
	auto const n = graph.num_vertices();
	auto const start_y = my_rank * n / num_threads;
	auto const end_y = (my_rank + 1) * n / num_threads;
	auto temp_dist = DistanceMat{n, end_y - start_y};

	init(graph, dist, temp_dist, start_y, end_y);

	for (auto k = 0; k < n; k++) {
		barrier.wait();
		calculate_distance(dist, temp_dist, n, start_y, end_y, pred, k);

		barrier.wait();
		update_distance(dist, temp_dist, n, start_y, end_y);
	}
}
