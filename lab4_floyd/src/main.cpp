/**
 *	Brandon Staab<bls114@zips.uakron.edu>
 *	Parallel Processing
 *	Dr. O'Neil
 *	22 Nov 2018
 *
 *	Lab 4: Floyd's Algorithm
 */

#include "do_benchmark.hpp"
#include "get_path.hpp"
#include "load_file.hpp"
#include "user_interface_fwd.hpp"

#include <chrono>
#include <iostream>


int main() noexcept {
	auto filename = prompt_filename("nqmq.dat");

	try {
		auto city_graph = load_file(filename);
		do_benchmark(city_graph);
		display_menu(city_graph);

		do {
			auto [from, to] = get_choice(city_graph.num_vertices() - 1);

			auto start = std::chrono::steady_clock::now();
			auto [path, distance] = get_path(city_graph, from, to, get_thead_amt(city_graph.num_vertices()));
			auto end = std::chrono::steady_clock::now();

			auto time_span = std::chrono::duration<double, std::milli>(end - start);
			print_results(city_graph, path, distance, time_span.count());
		} while (is_being_used());
	} catch (const std::exception& e) {
		std::cerr << "exception caught: " << e.what() << '\n';
		std::cout << "Please ensure that " << filename << " is of a valid format" << std::endl;

		return EXIT_FAILURE;
	}
}
