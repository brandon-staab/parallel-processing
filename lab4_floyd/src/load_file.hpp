/**
 *	Brandon Staab<bls114@zips.uakron.edu>
 *	Parallel Processing
 *	Dr. O'Neil
 *	22 Nov 2018
 *
 *	Lab 4: Floyd's Algorithm
 */

#pragma once

#include "CityGraph.hpp"


using Lines = std::vector<std::string>;


CityGraph load_file(std::string const filename);
