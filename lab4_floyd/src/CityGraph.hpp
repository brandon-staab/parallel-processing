/**
 *	Brandon Staab<bls114@zips.uakron.edu>
 *	Parallel Processing
 *	Dr. O'Neil
 *	22 Nov 2018
 *
 *	Lab 4: Floyd's Algorithm
 */

#pragma once

#include <MatrixGraph.hpp>

#include <string>


using CityName = std::string;
using Distance = double;
using CityGraph = MatrixGraph<std::string, Distance>;
using PathList = std::vector<std::size_t>;
