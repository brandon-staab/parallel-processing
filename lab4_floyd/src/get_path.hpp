/**
 *	Brandon Staab<bls114@zips.uakron.edu>
 *	Parallel Processing
 *	Dr. O'Neil
 *	22 Nov 2018
 *
 *	Lab 4: Floyd's Algorithm
 */

#pragma once

#include "CityGraph.hpp"

#include <cstddef>
#include <utility>


std::pair<PathList, Distance> get_path(CityGraph const& city_graph, std::size_t const from, std::size_t const to, std::size_t const num_threads) noexcept;
