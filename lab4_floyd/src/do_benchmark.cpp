/**
 *	Brandon Staab<bls114@zips.uakron.edu>
 *	Parallel Processing
 *	Dr. O'Neil
 *	22 Nov 2018
 *
 *	Lab 4: Floyd's Algorithm
 */

#include "do_benchmark.hpp"

#include "get_path.hpp"

#include <chrono>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <thread>


namespace {
	bool get_choice() noexcept {
		char choice;

	get_choice:
		std::cout << "Run benchmark (y/n)? " << std::flush;
		std::cin >> choice;
		if (choice == 'y') {
			return true;
		} else if (choice == 'n') {
			return false;
		} else {
			goto get_choice;
		}
	}

}  // namespace


void do_benchmark(CityGraph const& city_graph) noexcept {
	std::cout << "\nDetected " << std::thread::hardware_concurrency() << " hardware threads on this system\n";

	if (!get_choice()) {
		std::cout << '\n';
		return;
	}

	std::cout << "\nBenchmark\n- - - - -\n";
	for (auto i = 1; i <= city_graph.num_vertices(); i++) {
		std::cout << i << " thread(s) averages " << std::flush;

		auto constexpr step_amt = 16;
		std::size_t n = 0;
		double old_total = 0.0;
		double total = 0.0;
		double delta = 0.0;
		std::chrono::steady_clock::time_point start;
		std::chrono::steady_clock::time_point end;
		do {
			old_total = total;
			for (auto j = 0; j < step_amt; j++) {
				start = std::chrono::steady_clock::now();
				get_path(city_graph, 0, 0, i);
				end = std::chrono::steady_clock::now();

				n++;
				total += std::chrono::duration<double, std::milli>(end - start).count();
			}

			delta = (old_total / (n - step_amt)) - (total / n);
		} while (!std::isnormal(delta) || std::abs(delta) > 0.001);

		std::cout << std::setprecision(4) << std::fixed << (total / n) << " milliseconds after " << n << " tries\n";
	}

	std::cout << std::endl;
}
