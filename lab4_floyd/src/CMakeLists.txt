find_package (Threads)

add_executable(NQMP
	main.cpp

	user_interface/display_menu.cpp
	user_interface/get_choice.cpp
	user_interface/get_thread_amt.cpp
	user_interface/is_being_used.cpp
	user_interface/print_results.cpp
	user_interface/prompt_filename.cpp

	do_benchmark.cpp
	get_path.cpp
	load_file.cpp
	roy_floyd.cpp
)
set_property(TARGET NQMP PROPERTY CXX_STANDARD 17)
target_include_directories(NQMP PUBLIC
	./
	user_interface
)
target_link_libraries (NQMP
	${CMAKE_THREAD_LIBS_INIT}
	Barrier
	MatrixGraph
)
