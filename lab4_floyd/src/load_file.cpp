/**
 *	Brandon Staab<bls114@zips.uakron.edu>
 *	Parallel Processing
 *	Dr. O'Neil
 *	22 Nov 2018
 *
 *	Lab 4: Floyd's Algorithm
 */

#include "load_file.hpp"

#include <fstream>
#include <iostream>
#include <limits>
#include <stdexcept>


namespace {
	struct Road {
		std::size_t start_city;
		std::size_t end_city;
		double distance;
	};


	void operator>>(std::string const& str, Road& road) {
		std::size_t idx = 0;
		std::size_t offset = 0;

		road.start_city = std::stol(str, &offset);
		idx += offset;
		road.end_city = std::stol(str.substr(idx), &offset);
		idx += offset;
		road.distance = std::stod(str.substr(idx));
	}


	bool is_eof(std::string const str) { return str.find("-1 0 0") != std::string::npos; }
}  // namespace


CityGraph load_file(std::string const filename) {
	std::size_t line_num = 1;
	Road road;
	auto line = std::string{};
	auto ifs = std::ifstream{filename.c_str()};

	if (!ifs.is_open()) {
		std::string msg = "could not open file: " + filename;
		throw std::runtime_error(msg);
	}

	std::getline(ifs, line);
	auto const entries = std::stoul(line);

	auto city_graph = CityGraph{entries, std::numeric_limits<double>::infinity()};
	for (auto i = 0; i < entries; i++) {
		std::getline(ifs, line);

		line_num++;
		if (!ifs.good()) {
			throw std::runtime_error("illformed city on line " + std::to_string(line_num));
		}

		line.erase(std::remove(line.begin(), line.end(), '\r'), line.end());
		city_graph.add_vertex(line);
		city_graph.set_edge(i, i, 0);
	}

	while (ifs.good()) {
		std::getline(ifs, line);

		line_num++;
		if (!ifs.good()) {
			throw std::runtime_error("illformed on line " + std::to_string(line_num) + ", expected '-1 0 0' at the end of file");
		} else if (is_eof(line)) {
			break;
		}

		try {
			line >> road;
		} catch (const std::exception& e) {
			throw std::runtime_error("could not parse line" + std::to_string(line_num) + " because " + e.what());
		}

		if (road.start_city > entries) {
			throw std::runtime_error("start_city illformed on line " + std::to_string(line_num));
		} else if (road.end_city > entries) {
			throw std::runtime_error("end_city illformed on line " + std::to_string(line_num));
		}

		city_graph.set_edge(road.start_city, road.end_city, road.distance);
		city_graph.set_edge(road.end_city, road.start_city, road.distance);
	}

	return city_graph;
}
