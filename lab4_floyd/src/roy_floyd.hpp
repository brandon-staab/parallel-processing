/**
 *	Brandon Staab<bls114@zips.uakron.edu>
 *	Parallel Processing
 *	Dr. O'Neil
 *	22 Nov 2018
 *
 *	Lab 4: Floyd's Algorithm
 */

#pragma once

#include "CityGraph.hpp"

#include <Barrier.hpp>


int constexpr UNDEFINED = -1;

using DistanceMat = Matrix<Distance>;
using PredMat = Matrix<int>;


void roy_floyd(std::size_t const my_rank, std::size_t const num_threads, Barrier& barrier, CityGraph const& graph, DistanceMat& dist, PredMat& pred) noexcept;
