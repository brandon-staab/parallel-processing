/**
 *	Brandon Staab<bls114@zips.uakron.edu>
 *	Parallel Processing
 *	Dr. O'Neil
 *	8 Nov 2018
 *
 *	Lab 3: MPI Game of Life
 */

#include "check_cell.hpp"


namespace {
	enum class Direction : char {
		north,
		north_east,
		east,
		south_east,
		south,
		south_west,
		west,
		north_west,
	};


	int get_horizontal_offset(Direction const dir) {
		switch (dir) {
			case Direction::north_west:
			case Direction::west:
			case Direction::south_west:
				return -1;

			case Direction::north_east:
			case Direction::east:
			case Direction::south_east:
				return 1;

			default:
				return 0;
		}
	}


	int get_vertical_offset(Direction const dir) {
		switch (dir) {
			case Direction::north_west:
			case Direction::north:
			case Direction::north_east:
				return -1;

			case Direction::south_west:
			case Direction::south:
			case Direction::south_east:
				return 1;

			default:
				return 0;
		}
	}


	bool is_alive(Block const& block, std::size_t const x, std::size_t const y, Direction const dir) {
		auto tgt_x = static_cast<int>(x) + get_horizontal_offset(dir);
		auto tgt_y = static_cast<int>(y) + get_vertical_offset(dir);

		auto is_neighborly_ghost = [&]() {
			if (block.get_neighbors().north_east != UNDEFINED && tgt_x == block.get_columns() - 1 && tgt_y == 0) {
				return true;
			}

			if (block.get_neighbors().south_east != UNDEFINED && tgt_x == block.get_columns() - 1 && tgt_y == block.get_rows() - 1) {
				return true;
			}

			if (block.get_neighbors().south_west != UNDEFINED && tgt_x == 0 && tgt_y == block.get_rows() - 1) {
				return true;
			}

			if (block.get_neighbors().north_west != UNDEFINED && tgt_x == 0 && tgt_y == 0) {
				return true;
			}

			return false;
		};

		auto is_inbounds = [&]() {
			if (tgt_x < static_cast<int>(block.get_dimensions().left_bound)) {
				return false;
			}

			if (tgt_x > static_cast<int>(block.get_dimensions().right_bound)) {
				return false;
			}

			if (tgt_y < static_cast<int>(block.get_dimensions().top_bound)) {
				return false;
			}

			if (tgt_y > static_cast<int>(block.get_dimensions().bottom_bound)) {
				return false;
			}

			if ((tgt_x == 0 || tgt_x == block.get_columns() - 1) && (tgt_y == 0 || tgt_y == block.get_rows() - 1) && !is_neighborly_ghost()) {
				return true;
			}

			return true;
		};

		auto target_alive = [&]() { return block(tgt_x, tgt_y); };

		return is_inbounds() && target_alive();
	}


	auto count_neighbors(Block const& block, std::size_t const x, std::size_t const y) {
		auto neighbors = 0;

		for (auto i = 0; i < 8; i++) {
			neighbors += is_alive(block, x, y, static_cast<Direction>(i));
		}

		return neighbors;
	}

}  // namespace


bool check_cell(Block const& block, std::size_t const x, std::size_t const y) {
	auto neighbors = count_neighbors(block, x, y);

	if (block(x, y) == true) {
		if (neighbors == 2 || neighbors == 3) {
			return true;
		}
	} else if (neighbors == 3) {
		return true;
	}

	return false;
}
