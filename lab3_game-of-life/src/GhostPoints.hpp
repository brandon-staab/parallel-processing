/**
 *	Brandon Staab<bls114@zips.uakron.edu>
 *	Parallel Processing
 *	Dr. O'Neil
 *	8 Nov 2018
 *
 *	Lab 3: MPI Game of Life
 */

#pragma once

#include "Block.hpp"

#include <array>


using Segment = std::bitset<BLOCK_WIDTH>;


class GhostPoints : protected BoolMatrix2D<4, BLOCK_WIDTH + 2> {
	static_assert(BLOCK_WIDTH == BLOCK_HEIGHT);

  public:
	void update(Block& block);
	void calculate(Block const& block);
	void transfer(Block const& block);
	void apply(Block& block) const;

  private:
	Segment get_segment(std::size_t const direction) const;
	void apply_segment(Segment const& segment, std::size_t const direction);

  private:
	std::array<char, 4> m_received_corners{};
};
