/**
 *	Brandon Staab<bls114@zips.uakron.edu>
 *	Parallel Processing
 *	Dr. O'Neil
 *	8 Nov 2018
 *
 *	Lab 3: MPI Game of Life
 */

#pragma once

#include "BoolMatrix2D.hpp"


auto constexpr BLOCK_WIDTH = MATRIX_ROWS / 2;
auto constexpr BLOCK_HEIGHT = MATRIX_COLUMS / 2;
auto constexpr UNDEFINED = -1;

using BlockMatrix = BoolMatrix2D<BLOCK_WIDTH + 2, BLOCK_HEIGHT + 2>;


class Block : public BlockMatrix {
  public:
	struct Coordinates {
		std::size_t left_pos;
		std::size_t right_pos;
		std::size_t top_pos;
		std::size_t bottom_pos;
	};

	struct Dimensions {
		std::size_t left_bound;
		std::size_t right_bound;
		std::size_t top_bound;
		std::size_t bottom_bound;
	};

	struct Neighbors {
		int north;
		int north_east;
		int east;
		int south_east;
		int south;
		int south_west;
		int west;
		int north_west;
	};


	Block();
	Block(bool);

	Coordinates const& get_coordinates() const;
	Dimensions const& get_dimensions() const;
	Neighbors const& get_neighbors() const;

  private:
	Coordinates m_coordinates;
	Dimensions m_dimensions;
	Neighbors m_neighbors;
};
