/**
 *	Brandon Staab<bls114@zips.uakron.edu>
 *	Parallel Processing
 *	Dr. O'Neil
 *	8 Nov 2018
 *
 *	Lab 3: MPI Game of Life
 */

#include "load_file.hpp"

#include <mpi.h>

#include <fstream>
#include <stdexcept>


extern int MY_RANK;


namespace {
	struct Cell {
		int xpos;
		int ypos;
	};


	std::istream& operator>>(std::istream& is, Cell& cell) {
		is >> cell.xpos >> cell.ypos;
		return is;
	}


	Matrix read_file(char const* filename) {
		auto ifs = std::ifstream{filename};
		if (!ifs.is_open()) {
			throw std::runtime_error({"could not open file: ", filename});
		}

		auto cell = Cell{};
		auto matrix = Matrix{};
		while (ifs.good()) {
			ifs >> cell;

			if (ifs.eof()) {
				throw std::runtime_error("expected '-1 0' at the end of file");
			} else if (!ifs.good()) {
				throw std::runtime_error("bad input file");
			} else if (cell.xpos == -1 && cell.ypos == 0) {
				break;
			} else {
				matrix(cell.xpos, cell.ypos) = true;
			}
		}

		return matrix;
	}


	Block make_block(Matrix const& matrix) {
		auto block = Block{};

		for (auto y = 0; y < block.get_rows(); y++) {
			for (auto x = 0; x < block.get_columns(); x++) {
				auto const xpos = block.get_coordinates().left_pos + x - 1;
				auto const ypos = block.get_coordinates().top_pos + y - 1;

				if (xpos < 0 || xpos >= matrix.get_columns() || ypos < 0 || ypos >= matrix.get_rows()) {
					continue;
				} else if (matrix(xpos, ypos)) {
					block(x, y) = true;
				}
			}
		}

		return block;
	}
}  // namespace


Block load_file(char const* filename) {
	auto matrix = Matrix{};

	if (MY_RANK == 0) {
		try {
			matrix = read_file(filename);

			std::cout << "loaded:\n" << matrix << std::endl;
		} catch (std::exception& e) {
			std::cerr << "exception caught loading file: " << e.what() << '\n';
			MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
		}
	}

	MPI_Bcast(&matrix, sizeof(matrix), MPI_BYTE, 0, MPI_COMM_WORLD);

	return make_block(matrix);
}
