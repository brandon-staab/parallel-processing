/**
 *	Brandon Staab<bls114@zips.uakron.edu>
 *	Parallel Processing
 *	Dr. O'Neil
 *	8 Nov 2018
 *
 *	Lab 3: MPI Game of Life
 */

#include "tick_block.hpp"

#include "check_cell.hpp"


void tick_block(Block& block) {
	auto temp = BlockMatrix{};

	for (auto y = 1; y < block.get_rows() - 1; y++) {
		for (auto x = 1; x < block.get_columns() - 1; x++) {
			if (check_cell(block, x, y)) {
				temp(x, y) = true;
			}
		}
	}

	block.data() = std::move(temp.data());
}
