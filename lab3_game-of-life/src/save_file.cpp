/**
 *	Brandon Staab<bls114@zips.uakron.edu>
 *	Parallel Processing
 *	Dr. O'Neil
 *	8 Nov 2018
 *
 *	Lab 3: MPI Game of Life
 */

#include "save_file.hpp"

#include <mpi.h>

#include <fstream>


extern int MY_RANK;


namespace {
	std::size_t constexpr WORKING_THREADS = 4;


	void write_block(Matrix& matrix, Block const& block) {
		for (auto y = 1; y < block.get_rows() - 1; y++) {
			for (auto x = 1; x < block.get_columns() - 1; x++) {
				auto const xpos = block.get_coordinates().left_pos + x - 1;
				auto const ypos = block.get_coordinates().top_pos + y - 1;

				if (block(x, y)) {
					matrix(xpos, ypos) = true;
				}
			}
		}
	}


	Matrix gather(Block const& block) {
		auto status = MPI_Status{};
		auto matrix = Matrix{};

		if (MY_RANK == 0) {
			write_block(matrix, block);
		}

		for (auto i = 1; i < WORKING_THREADS; i++) {
			if (MY_RANK == 0) {
				auto recv_block = Block{false};
				MPI_Recv(&recv_block, sizeof(block), MPI_BYTE, i, i, MPI_COMM_WORLD, &status);
				write_block(matrix, recv_block);
			} else if (MY_RANK == i) {
				MPI_Send(&block, sizeof(block), MPI_BYTE, 0, MY_RANK, MPI_COMM_WORLD);
			}

			MPI_Barrier(MPI_COMM_WORLD);
		}

		return matrix;
	}


	void write_file(char const* filename, Matrix const& matrix) {
		auto ofs = std::ofstream{filename, std::ofstream::trunc};

		if (!ofs.is_open()) {
			std::cerr << "could not open file: " << filename << '\n';
			MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
		}

		for (auto y = 0; y < matrix.get_rows(); y++) {
			for (auto x = 0; x < matrix.get_columns(); x++) {
				if (matrix(x, y)) {
					ofs << x << ' ' << y << ' ';
				}
			}
		}
		ofs << "-1 0" << std::endl;
	}
}  // namespace


void save_file(char const* filename, Block const& block) {
	auto matrix = gather(block);

	if (MY_RANK == 0) {
		std::cout << "computed:\n" << matrix << std::endl;
		write_file(filename, matrix);
	}
}
