/**
 *	Brandon Staab<bls114@zips.uakron.edu>
 *	Parallel Processing
 *	Dr. O'Neil
 *	8 Nov 2018
 *
 *	Lab 3: MPI Game of Life
 */

#pragma once

#include "Block.hpp"


bool check_cell(Block const& block, std::size_t const x, std::size_t const y);
