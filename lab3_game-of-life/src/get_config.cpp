/**
 *	Brandon Staab<bls114@zips.uakron.edu>
 *	Parallel Processing
 *	Dr. O'Neil
 *	8 Nov 2018
 *
 *	Lab 3: MPI Game of Life
 */

#include "get_config.hpp"

#include <mpi.h>

#include <stdexcept>


extern int MY_RANK;


Config::Config(int const argc, char const* const* argv) : in_filename(argv[1]), out_filename(argc == 4 ? argv[3] : argv[1]), iterations(std::stoul(argv[2])) {
	if (argc == 3) {
		out_filename += "_output";
	}

	if (in_filename == out_filename) {
		throw std::invalid_argument("input and output file must be different");
	}
}


Config get_config(int const argc, char const* const* argv) {
	try {
		if (argc == 3 || argc == 4) {
			return Config(argc, argv);
		}

		throw std::invalid_argument("wrong number of parameters");
	} catch (std::exception& e) {
		if (MY_RANK == 0) {
			std::cerr << "exception caught getting config: " << e.what() << '\n';
			std::cout << "usage: " << argv[0] << " <input_file> <iterations> [output_file]" << std::endl;
		}

		MPI_Barrier(MPI_COMM_WORLD);
		MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
	}
}
