/**
 *	Brandon Staab<bls114@zips.uakron.edu>
 *	Parallel Processing
 *	Dr. O'Neil
 *	8 Nov 2018
 *
 *	Lab 3: MPI Game of Life
 */

#pragma once

#include <string>


struct Config {
	Config(int const argc, char const* const* argv);

	std::string in_filename;
	std::string out_filename;
	std::size_t iterations;
};


Config get_config(int const argc, char const* const* argv);
