/**
 *	Brandon Staab<bls114@zips.uakron.edu>
 *	Parallel Processing
 *	Dr. O'Neil
 *	8 Nov 2018
 *
 *	Lab 3: MPI Game of Life
 */

#pragma once

#include <bitset>
#include <cassert>
#include <iostream>


auto constexpr MATRIX_ROWS = 16;
auto constexpr MATRIX_COLUMS = MATRIX_ROWS;


template <std::size_t COLS, std::size_t ROWS>
class BoolMatrix2D {
	static_assert(COLS > 0);
	static_assert(ROWS > 0);
	using reference = typename std::bitset<COLS * ROWS>::reference;

  public:
	constexpr std::bitset<COLS * ROWS>& data() { return m_data; }
	constexpr std::bitset<COLS * ROWS> const& data() const { return m_data; }
	constexpr std::size_t get_columns() const { return COLS; }
	constexpr std::size_t get_rows() const { return ROWS; }
	constexpr std::size_t get_element_count() const { return m_data.size(); }

	constexpr reference operator()(std::size_t const column_x, std::size_t const row_y) {
		assert(0 <= column_x);
		assert(column_x < get_columns());
		assert(0 <= row_y);
		assert(row_y < get_rows());

		return m_data[row_y * COLS + column_x];
	}

	constexpr bool operator()(std::size_t const column_x, std::size_t const row_y) const {
		assert(0 <= column_x);
		assert(column_x < get_columns());
		assert(0 <= row_y);
		assert(row_y < get_rows());

		return m_data[row_y * COLS + column_x];
	}

  private:
	std::bitset<COLS * ROWS> m_data{};
};

/*****************************/

using Matrix = BoolMatrix2D<MATRIX_ROWS, MATRIX_COLUMS>;


template <std::size_t COLS, std::size_t ROWS>
std::ostream& operator<<(std::ostream& os, BoolMatrix2D<COLS, ROWS> const& matrix) {
	for (auto y = 0; y < matrix.get_columns(); y++) {
		for (auto x = 0; x < matrix.get_rows(); x++) {
			os << (matrix(x, y) ? '#' : '-');
		}
		os << '\n';
	}

	return os;
}
