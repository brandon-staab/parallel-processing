/**
 *	Brandon Staab<bls114@zips.uakron.edu>
 *	Parallel Processing
 *	Dr. O'Neil
 *	8 Nov 2018
 *
 *	Lab 3: MPI Game of Life
 */

#include "GhostPoints.hpp"
#include "get_config.hpp"
#include "load_file.hpp"
#include "save_file.hpp"
#include "tick_block.hpp"

#include <mpi.h>


int MY_RANK;
int THREAD_COUNT;


int main(int const argc, char const* argv[]) {
	MPI_Init(const_cast<int*>(&argc), const_cast<char***>(&argv));
	MPI_Comm_rank(MPI_COMM_WORLD, const_cast<int*>(&MY_RANK));
	MPI_Comm_size(MPI_COMM_WORLD, const_cast<int*>(&THREAD_COUNT));

	if (THREAD_COUNT < 4) {
		if (MY_RANK == 0) {
			std::cerr << "this program requires 4 threads to run\n";
		}

		MPI_Finalize();
		return EXIT_FAILURE;
	} else if (MY_RANK > 3) {
		std::cerr << "thread " << MY_RANK << " had no work to compute\n";
		MPI_Finalize();
		return EXIT_SUCCESS;
	}

	auto const config = get_config(argc, argv);
	auto block = load_file(config.in_filename.c_str());
	auto ghost_pts = GhostPoints{};

	for (auto step = 0; step < config.iterations; step++) {
		tick_block(block);
		ghost_pts.update(block);
	}

	save_file(config.out_filename.c_str(), block);
	MPI_Finalize();
}
