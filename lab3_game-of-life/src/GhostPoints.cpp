/**
 *	Brandon Staab<bls114@zips.uakron.edu>
 *	Parallel Processing
 *	Dr. O'Neil
 *	8 Nov 2018
 *
 *	Lab 3: MPI Game of Life
 */

#include "GhostPoints.hpp"

#include <mpi.h>

#include <cassert>
#include <vector>


extern int MY_RANK;

auto constexpr NORTH = 0;
auto constexpr NORTH_EAST = 0;
auto constexpr SOUTH = 1;
auto constexpr SOUTH_EAST = 1;
auto constexpr WEST = 2;
auto constexpr SOUTH_WEST = 2;
auto constexpr EAST = 3;
auto constexpr NORTH_WEST = 3;


void GhostPoints::update(Block& block) {
	calculate(block);
	transfer(block);
	apply(block);
}


void GhostPoints::calculate(Block const& block) {
	assert(get_rows() == block.get_rows());
	assert(get_rows() == block.get_columns());

	for (size_t x = 0; x < block.get_columns(); x++) {
		operator()(NORTH, x) = block(x, 1);
		operator()(SOUTH, x) = block(x, block.get_rows() - 2);
	}

	for (size_t y = 0; y < block.get_rows(); y++) {
		operator()(WEST, y) = block(1, y);
		operator()(EAST, y) = block(block.get_columns() - 2, y);
	}
}


void GhostPoints::transfer(Block const& block) {
	auto requests = std::vector<MPI_Request>{};
	auto statuses = std::vector<MPI_Status>{};
	std::array<Segment, 4> sending{};
	std::array<Segment, 4> received{};
	std::array<char, 4> sendind_corners{
		block(block.get_rows() - 2, 1),						   // north_east
		block(block.get_columns() - 2, block.get_rows() - 2),  // south_east
		block(1, block.get_columns() - 2),					   // south_west
		block(1, 1),										   // north_west
	};

	for (auto i = 0; i < sending.size(); i++) {
		sending[i] = get_segment(i);
	}

	if (block.get_neighbors().north != UNDEFINED) {
		MPI_Isend(&sending[NORTH], sizeof(Segment), MPI_BYTE, block.get_neighbors().north, SOUTH, MPI_COMM_WORLD, &*requests.emplace(requests.end()));
		MPI_Irecv(&received[NORTH], sizeof(Segment), MPI_BYTE, block.get_neighbors().north, NORTH, MPI_COMM_WORLD, &*requests.emplace(requests.end()));
	}

	if (block.get_neighbors().north_east != UNDEFINED) {
		MPI_Isend(&sendind_corners[NORTH_EAST], 1, MPI_BYTE, block.get_neighbors().north_east, SOUTH_WEST, MPI_COMM_WORLD, &*requests.emplace(requests.end()));
		MPI_Irecv(&m_received_corners[NORTH_EAST], 1, MPI_BYTE, block.get_neighbors().north_east, NORTH_EAST, MPI_COMM_WORLD,
				  &*requests.emplace(requests.end()));
	}

	if (block.get_neighbors().east != UNDEFINED) {
		MPI_Isend(&sending[EAST], sizeof(Segment), MPI_BYTE, block.get_neighbors().east, WEST, MPI_COMM_WORLD, &*requests.emplace(requests.end()));
		MPI_Irecv(&received[EAST], sizeof(Segment), MPI_BYTE, block.get_neighbors().east, EAST, MPI_COMM_WORLD, &*requests.emplace(requests.end()));
	}

	if (block.get_neighbors().south_east != UNDEFINED) {
		MPI_Isend(&sendind_corners[SOUTH_EAST], 1, MPI_BYTE, block.get_neighbors().south_east, NORTH_WEST, MPI_COMM_WORLD, &*requests.emplace(requests.end()));
		MPI_Irecv(&m_received_corners[SOUTH_EAST], 1, MPI_BYTE, block.get_neighbors().south_east, SOUTH_EAST, MPI_COMM_WORLD,
				  &*requests.emplace(requests.end()));
	}

	if (block.get_neighbors().south != UNDEFINED) {
		MPI_Isend(&sending[SOUTH], sizeof(Segment), MPI_BYTE, block.get_neighbors().south, NORTH, MPI_COMM_WORLD, &*requests.emplace(requests.end()));
		MPI_Irecv(&received[SOUTH], sizeof(Segment), MPI_BYTE, block.get_neighbors().south, SOUTH, MPI_COMM_WORLD, &*requests.emplace(requests.end()));
	}

	if (block.get_neighbors().south_west != UNDEFINED) {
		MPI_Isend(&sendind_corners[SOUTH_WEST], 1, MPI_BYTE, block.get_neighbors().south_west, NORTH_EAST, MPI_COMM_WORLD, &*requests.emplace(requests.end()));
		MPI_Irecv(&m_received_corners[SOUTH_WEST], 1, MPI_BYTE, block.get_neighbors().south_west, SOUTH_WEST, MPI_COMM_WORLD,
				  &*requests.emplace(requests.end()));
	}

	if (block.get_neighbors().west != UNDEFINED) {
		MPI_Isend(&sending[WEST], sizeof(Segment), MPI_BYTE, block.get_neighbors().west, EAST, MPI_COMM_WORLD, &*requests.emplace(requests.end()));
		MPI_Irecv(&received[WEST], sizeof(Segment), MPI_BYTE, block.get_neighbors().west, WEST, MPI_COMM_WORLD, &*requests.emplace(requests.end()));
	}

	if (block.get_neighbors().north_west != UNDEFINED) {
		MPI_Isend(&sendind_corners[NORTH_WEST], 1, MPI_BYTE, block.get_neighbors().north_west, SOUTH_EAST, MPI_COMM_WORLD, &*requests.emplace(requests.end()));
		MPI_Irecv(&m_received_corners[NORTH_WEST], 1, MPI_BYTE, block.get_neighbors().north_west, NORTH_WEST, MPI_COMM_WORLD,
				  &*requests.emplace(requests.end()));
	}

	MPI_Waitall(requests.size(), requests.data(), statuses.data());

	for (auto i = 0; i < received.size(); i++) {
		apply_segment(received[i], i);
	}
}


void GhostPoints::apply(Block& block) const {
	assert(get_rows() == block.get_rows());
	assert(get_rows() == block.get_columns());

	for (size_t x = 0; x < block.get_columns(); x++) {
		block(x, 0) = operator()(NORTH, x);
		block(x, block.get_rows() - 1) = operator()(SOUTH, x);
	}

	for (size_t y = 0; y < block.get_rows(); y++) {
		block(0, y) = operator()(WEST, y);
		block(block.get_columns() - 1, y) = operator()(EAST, y);
	}

	block(block.get_rows() - 1, 0) = static_cast<bool>(m_received_corners[NORTH_EAST]);
	block(block.get_columns() - 1, block.get_rows() - 1) = static_cast<bool>(m_received_corners[SOUTH_EAST]);
	block(0, block.get_columns() - 1) = static_cast<bool>(m_received_corners[SOUTH_WEST]);
	block(0, 0) = static_cast<bool>(m_received_corners[NORTH_WEST]);
}


Segment GhostPoints::get_segment(std::size_t const direction) const {
	assert(direction >= 0);
	assert(direction <= 3);

	auto segment = Segment{};
	for (auto i = 0; i < segment.size(); i++) {
		segment[i] = operator()(direction, i + 1);
	}

	return segment;
}


void GhostPoints::apply_segment(Segment const& segment, std::size_t const direction) {
	assert(direction >= 0);
	assert(direction <= 3);

	for (auto i = 0; i < segment.size(); i++) {
		operator()(direction, i + 1) = segment[i];
	}
}
