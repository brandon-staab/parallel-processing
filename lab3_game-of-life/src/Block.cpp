/**
 *	Brandon Staab<bls114@zips.uakron.edu>
 *	Parallel Processing
 *	Dr. O'Neil
 *	8 Nov 2018
 *
 *	Lab 3: MPI Game of Life
 */

#include "Block.hpp"


extern int MY_RANK;

auto constexpr THREAD_PER_WIDTH = MATRIX_ROWS / BLOCK_WIDTH;
auto constexpr THREAD_PER_HEIGHT = MATRIX_COLUMS / BLOCK_HEIGHT;


static_assert(MATRIX_ROWS % THREAD_PER_WIDTH == 0);
static_assert(MATRIX_COLUMS % THREAD_PER_HEIGHT == 0);


namespace {
	inline int x() { return MY_RANK % THREAD_PER_WIDTH; }
	inline int y() { return MY_RANK / THREAD_PER_HEIGHT; }

	inline std::size_t x_pos_left() { return x() * BLOCK_WIDTH; }
	inline std::size_t x_pos_right() { return x_pos_left() + BLOCK_WIDTH - 1; }
	inline std::size_t y_pos_top() { return y() * BLOCK_HEIGHT; }
	inline std::size_t y_pos_bottom() { return y_pos_top() + BLOCK_HEIGHT - 1; }

	inline std::size_t dim_left() { return (x_pos_left() == 0) ? 1 : 0; }
	inline std::size_t dim_right() { return (x_pos_right() + 1 >= MATRIX_COLUMS) ? (BLOCK_WIDTH) : (BLOCK_WIDTH + 1); }
	inline std::size_t dim_top() { return (y_pos_top() == 0) ? 1 : 0; }
	inline std::size_t dim_bottom() { return (y_pos_bottom() + 1 >= MATRIX_ROWS) ? (BLOCK_HEIGHT) : (BLOCK_HEIGHT + 1); }

	auto coords_to_rank(int const x, int const y) {
		if (x < 0 || x >= THREAD_PER_WIDTH) {
			return UNDEFINED;
		}

		if (y < 0 || y >= THREAD_PER_HEIGHT) {
			return UNDEFINED;
		}

		return y * THREAD_PER_WIDTH + x;
	}
}  // namespace


Block::Block()
	: BlockMatrix(),
	  m_coordinates{
		  x_pos_left(),
		  x_pos_right(),
		  y_pos_top(),
		  y_pos_bottom(),
	  },
	  m_dimensions{
		  dim_left(),
		  dim_right(),
		  dim_top(),
		  dim_bottom(),
	  },
	  m_neighbors{
		  coords_to_rank(x(), y() - 1),		 // north
		  coords_to_rank(x() + 1, y() - 1),  // north_east
		  coords_to_rank(x() + 1, y()),		 // east
		  coords_to_rank(x() + 1, y() + 1),  // south_east
		  coords_to_rank(x(), y() + 1),		 // south
		  coords_to_rank(x() - 1, y() + 1),  // south_west
		  coords_to_rank(x() - 1, y()),		 // west
		  coords_to_rank(x() - 1, y() - 1),  // north_west
	  } {}


Block::Block(bool) : BlockMatrix(), m_coordinates(), m_dimensions(), m_neighbors() {}


Block::Coordinates const& Block::get_coordinates() const {
	return m_coordinates;
}


Block::Dimensions const& Block::get_dimensions() const {
	return m_dimensions;
}


Block::Neighbors const& Block::get_neighbors() const {
	return m_neighbors;
}
