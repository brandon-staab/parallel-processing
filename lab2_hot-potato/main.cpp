/**
 *	Brandon Staab<bls114@zips.uakron.edu>
 *	Parallel Processing
 *	Dr. O'Neil
 *	18 October 2018
 */

#include "mpi.h"

#include <functional>
#include <random>


int main(int argc, char* argv[]) {
	int my_rank, thread_count, potato, next_node;
	std::random_device rd;
	std::default_random_engine generator(rd());
	MPI_Status status;

	// mpi setup
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
	MPI_Comm_size(MPI_COMM_WORLD, &thread_count);

	// random number setup
	std::uniform_int_distribution<int> distribution_next(0, thread_count - 1);
	auto dice = std::bind(distribution_next, generator);

	if (my_rank == 0) {  // have the root node get the potato going
		// pick someone to pass the potato to first
		potato = std::uniform_int_distribution<int>(thread_count, 1 * thread_count)(generator);
		next_node = dice();
		std::cout << "Node " << my_rank << " has the potato, passing to node " << next_node << std::endl;

		// pass the potato
		MPI_Send(&potato, 1, MPI_INT, next_node, 0, MPI_COMM_WORLD);
	}

	do {  // pass the potato around
		// get ready to catch the potato
		MPI_Recv(&potato, 1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

		if (--potato == 0) {  // check if the potato has cooled off yet
			std::cout << "Node " << my_rank << " is it, game over." << std::endl;

			// tell everyone else that the game is over
			for (auto i = 0; i < thread_count; i++) {
				MPI_Send(&potato, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
			}
		}

		if (potato > 0) {  // if the potato is hot, pass it again
			// pick whose next
			next_node = dice();
			std::cout << "Node " << my_rank << " has the potato, passing to node " << next_node << std::endl;

			// pass the potato
			MPI_Send(&potato, 1, MPI_INT, next_node, 0, MPI_COMM_WORLD);
		}
	} while (potato > 0);

	MPI_Finalize();
}
