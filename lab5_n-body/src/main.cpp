#include "load_file.hpp"

#include "ui_fwd.hpp"

#include <omp.h>

#include <iostream>


std::string const default_filename = "../nbodies.dat";
extern Force const G = 1.0;


int main(int argc, char const* argv[]) {
	try {
		auto filename = prompt_filename(default_filename);
		auto universe = load_file(filename, 1000);

		std::cout << "Starting positions\n";
		universe.print(std::cout, 0, 8);

		omp_set_num_threads(40);
		universe.simulate(100, 0.1);

		std::cout << "Ending positions\n";
		universe.print(std::cout, 0, 8);
		universe.print(std::cout, 980, 20);
	} catch (std::runtime_error const& e) {
		std::cerr << e.what() << '\n';
		return EXIT_FAILURE;
	}
}
