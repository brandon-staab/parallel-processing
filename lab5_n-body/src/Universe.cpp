#include "Universe.hpp"

#include <omp.h>

#include <cassert>
#include <cmath>
#include <iostream>
#include <utility>
#include <iomanip>


extern Force const G;


void Universe::add_particle(Position const p_pos, Velocity const p_vel, Mass const p_mass) {
	m_positions.push_back(p_pos);
	m_velocities.push_back(p_vel);
	m_masses.push_back(p_mass);
}


void Universe::simulate(std::size_t const p_steps, double const p_step_size) {
	for (auto t = 0; t < p_steps; t++) {
		std::cerr << t << ' ';
		tick(p_step_size);
	}
	std::cerr << '\n';
}


ForceVecs Universe::calc_forces() const {
	auto forces = ForceVecs{size()};
	auto forces_qk = ForceVecs{size()};
	auto forces_local = std::vector<ForceVecs>{static_cast<std::size_t>(omp_get_num_threads()), ForceVecs{size()}};

	#pragma omp parallel for
	for (auto q = 0; q < size(); q++) {
		auto forces_qk = ForceVec{};
		#pragma omp parallel for
		for (auto k = q + 1; k < size(); k++) {
			auto diff = m_positions[q] - m_positions[k];
			auto dist = std::sqrt(diff.x * diff.x + diff.y * diff.y);
			auto dist_cubed = dist * dist * dist;
			forces_qk = G * m_masses[q] * m_masses[k] / dist_cubed * diff;
			forces_local[omp_get_thread_num()][q] += forces_qk;
			forces_local[omp_get_thread_num()][k] -= forces_qk;
		}
	}

	#pragma omp barrier

	#pragma omp parallel for
	for (auto q = 0; q < size(); q++) {
		#pragma omp parallel for
		for (auto thread = 0; thread < omp_get_num_threads(); thread++) {
			forces[q] += forces_local[omp_get_thread_num()][q];
		}
	}

	return forces;
}


inline void Universe::tick(double const p_dt) {
	auto forces = calc_forces();

	#pragma omp parallel for
	for (auto i = 0; i < size(); i++) {
		m_velocities[i] = m_velocities[i] + forces[i] * p_dt / m_masses[i];
		m_positions[i] = m_positions[i] + m_velocities[i] * p_dt;
	}
}


void Universe::print(std::ostream& p_os) const {
	p_os << std::setprecision(4);
	for (size_t i = 0; i < size(); i++) {
		p_os << "idx = (" << std::setw(4) << i <<
		");   pos = (" << std::setw(10) << m_positions[i].x << ", " << std::setw(10) << m_positions[i].y <<
		");   vel = (" << std::setw(10) << m_velocities[i].x << ", " << std::setw(10) << m_velocities[i].y <<
		");   mass = (" << std::setw(10) << m_masses[i] << ");\n";
	}
}


void Universe::print(std::ostream& p_os, std::size_t const p_start, std::size_t const p_size) const {
	assert(p_start + p_size <= size());

	p_os  << std::fixed<< std::setprecision(4);
	for (size_t i = p_start; i < p_start + p_size; i++) {
		p_os << "idx = (" << std::setw(4) << i <<
		");   pos = (" << std::setw(10) << m_positions[i].x << ", " << std::setw(10) << m_positions[i].y <<
		");   vel = (" << std::setw(10) << m_velocities[i].x << ", " << std::setw(10) << m_velocities[i].y <<
		");   mass = (" << std::setw(10) << m_masses[i] << ");\n";
	}
}


std::size_t Universe::size() const {
	assert(m_positions.size() == m_velocities.size());
	assert(m_positions.size() == m_masses.size());

	return m_positions.size();
}
