#pragma once

#include "Universe.hpp"

#include <string>


Universe load_file(std::string const p_filename, std::size_t const p_amt);
