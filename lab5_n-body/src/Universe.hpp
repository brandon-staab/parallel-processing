#pragma once

#include <Vector2D.hpp>

#include <iosfwd>
#include <vector>


using Force = double;
using ForceVec = Vector2D<Force>;
using ForceVecs = std::vector<ForceVec>;
using Mass = double;
using Masses = std::vector<Mass>;
using Distance = double;
using Position = Vector2D<Distance>;
using Positions = std::vector<Position>;
using Velocity = Vector2D<Distance>;
using Velocities = std::vector<Velocity>;


class Universe {
  public:
	void add_particle(Position const p_pos, Velocity const p_vel, Mass const p_mass);
	void simulate(std::size_t const p_steps, double const p_step_size);
	ForceVecs calc_forces() const;
	inline void tick(double const p_dt);

	void print(std::ostream& p_os) const;
	void print(std::ostream& p_os, std::size_t const p_start, std::size_t const p_size) const;
	std::size_t size() const;

  private:
	Positions m_positions;
	Masses m_masses;
	Velocities m_velocities;
};
