#include "load_file.hpp"

#include <fstream>
#include <stdexcept>
#include <string>

namespace {
	Position line_to_pos(std::string const p_line) {
		Position pos;
		std::size_t idx = 0;
		std::size_t offset = 0;

		pos.x = std::stod(p_line, &offset);
		idx += offset;
		pos.y = std::stod(p_line.substr(idx), &offset);

		return pos;
	}
}  // namespace

Universe load_file(std::string const p_filename, std::size_t const p_amt) {
	auto ifs = std::ifstream{p_filename.c_str()};
	if (!ifs.is_open()) {
		throw std::runtime_error{p_filename + " could not be opened"};
	}

	std::size_t num_line = 0;
	std::string line;
	auto universe = Universe{};
	try {
		while (ifs.good() && num_line < p_amt) {
			num_line++;
			std::getline(ifs, line);
			if (!line.empty()) {
				universe.add_particle(line_to_pos(line), Velocity{0, 0}, Mass{1});
			}
		}
	} catch (std::invalid_argument const& e) {
		std::string msg = p_filename;
		msg += " could not be read on line ";
		msg += std::to_string(num_line);
		msg += " because of an invalid argument";
		throw std::runtime_error{msg};
	} catch (std::out_of_range const& e) {
		std::string msg = p_filename;
		msg += " could not be read on line ";
		msg += std::to_string(num_line);
		msg += " because input was out of range";
		throw std::runtime_error{msg};
	}

	if (universe.size() != p_amt) {
		std::string msg = "Expected ";
		msg += std::to_string(p_amt);
		msg += " particles, but only found ";
		msg += std::to_string(universe.size());
		throw std::runtime_error(msg);
	}

	return universe;
}
