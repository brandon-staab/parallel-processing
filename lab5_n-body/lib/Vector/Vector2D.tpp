#pragma once


template <typename T>
inline Vector2D<T>::Vector2D() : x(0), y(0) {}


template <typename T>
inline Vector2D<T>::Vector2D(T p_x, T p_y) : x(p_x), y(p_y) {}


template <typename T>
template <typename U>
inline Vector2D<T>::Vector2D(const Vector2D<U>& p_vec) : x(static_cast<T>(p_vec.x)), y(static_cast<T>(p_vec.y)) {}


template <typename T>
inline Vector2D<T> operator-(Vector2D<T> const& p_other) {
	return Vector2D<T>{-p_other.x, -p_other.y};
}


template <typename T>
inline Vector2D<T>& operator+=(Vector2D<T>& p_lhs, Vector2D<T> const& p_rhs) {
	p_lhs.x += p_rhs.x;
	p_lhs.y += p_rhs.y;

	return p_lhs;
}


template <typename T>
inline Vector2D<T>& operator-=(Vector2D<T>& p_lhs, Vector2D<T> const& p_rhs) {
	p_lhs.x -= p_rhs.x;
	p_lhs.y -= p_rhs.y;

	return p_lhs;
}


template <typename T>
inline Vector2D<T> operator+(Vector2D<T> const& p_lhs, Vector2D<T> const& p_rhs) {
	return Vector2D<T>{p_lhs.x + p_rhs.x, p_lhs.y + p_rhs.y};
}


template <typename T>
inline Vector2D<T> operator-(Vector2D<T> const& p_lhs, Vector2D<T> const& p_rhs) {
	return Vector2D<T>{p_lhs.x - p_rhs.x, p_lhs.y - p_rhs.y};
}


template <typename T>
inline Vector2D<T> operator*(Vector2D<T> const& p_lhs, T p_rhs) {
	return Vector2D<T>{p_lhs.x * p_rhs, p_lhs.y * p_rhs};
}


template <typename T>
inline Vector2D<T> operator*(T p_lhs, Vector2D<T> const& p_rhs) {
	return Vector2D<T>{p_rhs.x * p_lhs, p_rhs.y * p_lhs};
}


template <typename T>
inline Vector2D<T>& operator*=(Vector2D<T>& p_lhs, T p_rhs) {
	p_lhs.x *= p_rhs;
	p_lhs.y *= p_rhs;

	return p_lhs;
}


template <typename T>
inline Vector2D<T> operator/(Vector2D<T> const& p_lhs, T p_rhs) {
	return Vector2D<T>{p_lhs.x / p_rhs, p_lhs.y / p_rhs};
}


template <typename T>
inline Vector2D<T>& operator/=(Vector2D<T>& p_lhs, T p_rhs) {
	p_lhs.x /= p_rhs;
	p_lhs.y /= p_rhs;

	return p_lhs;
}


template <typename T>
inline bool operator==(Vector2D<T> const& p_lhs, Vector2D<T> const& p_rhs) {
	return (p_lhs.x == p_rhs.x) && (p_lhs.y == p_rhs.y);
}


template <typename T>
inline bool operator!=(Vector2D<T> const& p_lhs, Vector2D<T> const& p_rhs) {
	return (p_lhs.x != p_rhs.x) || (p_lhs.y != p_rhs.y);
}
