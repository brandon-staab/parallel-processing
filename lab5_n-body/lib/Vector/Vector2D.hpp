#pragma once


template <typename T>
struct Vector2D {
  public:
	Vector2D();
	Vector2D(T p_x, T p_y);

	template <typename U>
	explicit Vector2D(Vector2D<U> const& p_vec);

  public:
	T x;
	T y;
};


template <typename T>
Vector2D<T> operator-(Vector2D<T> const& p_other);


template <typename T>
Vector2D<T>& operator+=(Vector2D<T>& p_lhs, Vector2D<T> const& p_rhs);


template <typename T>
Vector2D<T>& operator-=(Vector2D<T>& p_lhs, Vector2D<T> const& p_rhs);


template <typename T>
Vector2D<T> operator+(Vector2D<T> const& p_lhs, Vector2D<T> const& p_rhs);


template <typename T>
Vector2D<T> operator-(Vector2D<T> const& p_lhs, Vector2D<T> const& p_rhs);


template <typename T>
Vector2D<T> operator*(Vector2D<T> const& p_lhs, T p_rhs);


template <typename T>
Vector2D<T> operator*(T p_lhs, Vector2D<T> const& p_rhs);


template <typename T>
Vector2D<T>& operator*=(Vector2D<T>& p_lhs, T p_rhs);


template <typename T>
Vector2D<T> operator/(Vector2D<T> const& p_lhs, T p_rhs);


template <typename T>
Vector2D<T>& operator/=(Vector2D<T>& p_lhs, T p_rhs);


template <typename T>
bool operator==(Vector2D<T> const& p_lhs, Vector2D<T> const& p_rhs);


template <typename T>
bool operator!=(Vector2D<T> const& p_lhs, Vector2D<T> const& p_rhs);


#include "Vector2D.tpp"
